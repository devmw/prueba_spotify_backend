const express 	= require('express')
const mongo 	= require('mongodb')
const request 	= require('request')
const app 		= express()
const port 		= 3000


var MClient = require('mongodb').MongoClient
var url 	= 'mongodb://localhost:27017/Prueba_Spotify' 


/**----------------------------------/
*  -- CREACION DE LA DB Y SU COLLEC -- 
*/
MClient.connect(url, function(err, db) {
  	if (err) throw err
  	var dbo = db.db("Prueba_Spotify")
  	dbo.createCollection("albunes", function(err, res) {
    	if (err) throw err
    	db.close()
  	})
})


/**----------------------------------/
*  -- SE ACEPTAN PETICIONES DEL ORIGIN 4200 -- 
*/
app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});


/**----------------------------------/
*  -- PEDIR RECORDS DE SPOTIFY -- 
*/
function get_records_spotify() {
	var id = '4aawyAB9vmqN3uQ7FjRGTy'
	var tk = 'BQAgOTdH-ppsM16Qv5YYh_9vT8cFvjallcIAigAKGN4p-zwOZOasOzArhPodbBS0eCDZsXTdzGZHEd1ujSHL5JA_3oo3dzRJwg5jfinyx9grMX7W62lEkVRZ4_GM1NpJ6zdtEx3XpvR6gG4leQ69VNuvExLwyFM5cdtrtlE'

	var headers = {
	    'Content-Type'		: 'application/json',
	    'Accept'			: 'application/json',
	    'Authorization'		: 'Bearer '+tk
	}

	var options = {
	    url     : 'https://api.spotify.com/v1/albums/'+id+'?market=ES',
	    method  : 'GET',
	    headers : headers
	}

	request(options, function (error, response, body) {
	    if (!error && response.statusCode == 200) {
	        var body = JSON.parse(body)
	        var item = []
	        for (var i in body.tracks.items) {
	        	item.push(body.tracks.items[i])
	        }
	       	save_data_mongodb(item)
	    }
	})
}


/**----------------------------------/
*  -- GUARDAR RECORDS DE SPOTIFY -- 
*/
function save_data_mongodb(obj) {
	MClient.connect(url, (err, db) => {
		if (err) throw err
		var dbo = db.db("Prueba_Spotify")
		dbo.collection("albunes").drop()

		for (var i in obj) {
			dbo.collection("albunes").insertOne(obj[i], (err, res) => {
				if (err) throw err
				db.close()
			})
		}
	})
}


/* ----------------------------------/
/    -- PEDIR TODAS LOS ALBUNES -- 
*/
app.get('/get_a', (req, res) => {
	get_records_spotify()

	MClient.connect(url, (err, db) => {
	  	if (err) throw err
	  	var dbo 	= db.db("Prueba_Spotify")
	  	var cursor	= dbo.collection('albunes').find()
	  	var items 	= []
	  	var term 	= false
		
		cursor.each((err, item) => {
		    if(item == null) {
		    	term = true
		        db.close()
		        return
		    }
		    else{
		    	items.push(item)
		    }
		})
		
		var int = setInterval(() => {
			if (term) 	{ res.json({'data':items}); clearInterval(int) } 
			else 		{ res.json({'data':[]}); 	clearInterval(int) }
		},500)

	})

})


/* ----------------------------------/
/   -- PEDIR UN ALBUN POR NOMBRE -- 
*/
app.get('/get_a/:word', (req, res) => {
	var params = req.params

	MClient.connect(url, (err, db) => {
	  	if (err) throw err
	  	var dbo 	= db.db("Prueba_Spotify")
	  	var cursor	= dbo.collection('albunes').find()
	  	var items 	= []
	  	var items_f = []
	  	var term 	= false
		
		cursor.each((err, item) => {
		    if(item == null) {
		    	term = true
		        db.close()
		        return
		    }
		    else{
		    	items.push(item)
		    }
		})
		
		var int = setInterval(() => {
			if (term) {
				for (var i in items){
					var itn = items[i].name
					var spl = itn.split(' ')

					for (var x in spl){
						if (spl[x] == params.word) {
							items_f.push(items[i])
						}
					}
				}
				res.json({'data':items_f})
				clearInterval(int) 
			} 
			else { 
				res.json({'data':[]})
				clearInterval(int) 
			}
		},500)

	})

})


/* -----------------------------/
/   -- RUTA NO ESPECIFICADA -- 
*/
app.get('*', (req, res) => {
	res.status(404).send('Sorry, not found :c')
})


/* -----------------------------/
/    -- SERVIDOR EN MODO ON --
*/
app.listen(port, () => {
	console.log(` -- Corriendo por el puerto ${port} --`)
})

